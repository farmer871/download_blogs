#!/bin/bash
#
# edited by semigod
#
# 2020/03/24
# 2020/08/28

prepare () {
    echo -e "\n[檢查環境]\n"
    ## check url.file if exist
    if [[ ${count} -eq 1 ]]; then
        sourceUrlFile=${urlFile}
    else
        sourceUrlFile="${defaultUrlFile}"
    fi
    echo -e "- 下載清單檔案 [${sourceUrlFile}]"
    
    if [[ ! -f "${sourceUrlFile}" ]]; then
        echo -e "  <!> 無法存取URL下載網址檔案 [${sourceUrlFile}] <!>\n"
        exit
    else
        echo -e "  >>> [OK]\n"
    fi

    #filename=$(echo ${sourceUrlFile} | cut -d. -f1)
    
    reverseUrlFile="${urlFilename}.rr"
    titleTmpfile="${urlFilename}_title.tmp"
    titleFileTimeline="${urlFilename}_title.${timeline}"
    resultPath="RESULT_$(echo "${urlFilename}" | tr '[:lower:]' '[:upper:]')"

    ## check working path
    echo -e "- 下載目錄 [${resultPath}]"
    if [[ ! -d ${resultPath} ]]; then
        mkdir -p ${resultPath}
        echo -e "  >>> [建立完成]\n"
    else
        echo -e "  >>> [已存在]\n"
    fi

    ## clean up
    if [[ -f "${titleTmpfile}" ]]; then
        rm -f ${titleTmpfile}
    fi
    if [[ -f "${reverseUrlFile}" ]]; then
        rm -f ${reverseUrlFile}
    fi

    ## reverse file content
    nl ${sourceUrlFile} | sort -nr | cut -f 2- > ${reverseUrlFile}
}

dl2pdf () {
    ##
    echo -e "[下載轉換作業開始]\n"

    while read downloadUrl; do
        title=$(urlencode -d "${downloadUrl}" | sed -e 's|^https://.*vmware-taiwan/||' -e 's|/.*$||')
        file="${title}"
        echo "${file}" >> ${titleTmpfile}
        total=$(expr ${total} + 1)
        echo -e "  [${total}] <${file}>"

        targetPdfFile="./${resultPath}/${file}.pdf"
        if [[ -f "${targetPdfFile}" ]]; then
            echo -e "  >>> [已存在]\n"
        elif [[ ! -f "${targetPdfFile}" ]]; then
        #    wkhtmltopdf -n "${downloadUrl}" ${targetPdfFile} 2> /dev/null
            echo -e "  >>> [完成]\n"
        fi
    done < ${reverseUrlFile}

    echo -e "[作業完成]\n"
}

clean () {
    if [[ -f "${titleTmpfile}" ]]; then
        mv ${titleTmpfile} ${titleFileTimeline}
    fi
    if [[ -f "${reverseUrlFile}" ]]; then
        rm -f ${reverseUrlFile}
    fi
    echo -e "[結果顯示]\n"
    echo -e "  > 檔案列表清單 [${titleFileTimeline}]"
    echo -e "  > 檔案下載目錄 [${resultPath}]"
    echo ""
}

main () {
    prepare
    dl2pdf
    clean
}

count=$#
urlFilePath='../notes'
urlFile="${urlFilePath}/$1"
urlFilename=$1
timeline=$(date +%Y%m%d)
defaultUrlFile="nsx.notes"
#resultPath="RESULT_$(echo "${sourceUrlFile}" | cut -d. -f1 | tr '[:lower:]' '[:upper:]')"
total=0

main