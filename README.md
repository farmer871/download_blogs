# [快速指南] 下載網誌文章並轉成PDF備存
將VMware Taiwan@Facebook的優質網誌文章，下載並直接轉換成PDF檔案。

## [目的]

VMware Taiwan在Facebook網誌有很多文章，內容極為豐富，這個專案重點就是想下載成PDF檔，作為之後參考及研讀。
透過文章連結的收集(為一個文字檔)，再根據此文字檔，將檔案從Facebook下載，並同時轉換成PDF檔案儲存。

 ![001.png](./pics/001.png)


## [任務]

- 手動複製FB網誌文章連結
- 安裝wkhtmltopdf工具
- 確認下載及轉換流程
- 撰寫基本BASH程式
- 進行測試

## [執行步驟]

1. 這邊是以 **<網路虛擬化NSX技術文章系列>** 為主，並先針對系列文章進行分類，取得連
結資訊，**手動**儲存成一個下載網址清單。

 基本上就是將有興趣的網誌文章連結，使用滑鼠右鍵取得**網頁連結**。

 ![002.png](./pics/002.png)

2. 存成一個**下載網址文字檔**。此範例為 *NSX-Microsegement-Deployment.notes*

  > 下載文字檔名稱可自訂。

 ![003.png](./pics/003.png)

 以上是以 **<網路虛擬化NSX-技術文章系列:微分段部署實務方法分享>** 系列文章作為測試及說明。
 > 這部份目前沒有能力使用爬蟲技巧取得Facebook內容，如果有高手知道，可以指導一下。

3. 從網頁取得的文字連結會使用**url編碼**，這樣一來下載檔案無法使用此命名。需要先將下載連結解碼，並取出**文章名稱**，作為轉換後的**PDF檔名**。

 可以使用以下範例將**文章名稱**從複製的URL連結中取出。

 ```bash
  $ urlencode -d "${downloadUrl}" | sed -e 's|^https://.*vmware-taiwan/||' -e 's|/.*$||')
 ```
 > ${downloadUrl}是之前複製儲存的連結，可透過程式讀入存成變數。

4. 接著就是透過複製儲存的URL連結，使用[[wkhtmltopdf]](https://wkhtmltopdf.org/ "wkhtmltopdf")工具便可成。這個工具支援Windows, MacOS及Linux平台。

 ![004.png](./pics/004.png)

 請根據自己的作業平台下載安裝。

 ```bash
  $ sudo dnf install wkhtmltopdf
 ```

5. 使用方式粉簡單，直接執行 `wkhtmltopdf` 命令即可。

 以下範例表示擷取 `http://google.com` 網頁內容，並轉換成 `google.pdf` 。

 ```bash
  $ wkhtmltopdf http://google.com google.pdf
 ```

6. 將剛剛轉換的**PDF檔**儲存於**指定目錄**，**目錄名稱**依據**下載清單文字檔**，可透過程式撰寫指定為**RESULT_{下載清單文字檔}**。

 我們下載文字檔名稱 ***NSX-Microsegement-Deployment.notes***，轉換PDF檔會儲存在
 ***RESULT_NSX-MICROSEGEMENT-DEPLOYMENT***

 ```bash
  $ targetPdfFile="./${resultPath}/${file}.pdf"
  $ wkhtmltopdf -n "${downloadUrl}" ${targetPdfFile} 2> /dev/null
 ```
 > -n, --disable-javascript 不允許網頁執行javascript

7. 大致上準備工作差不多了，根據上述概念步驟撰寫基本BASH程式。請參考[[範例程式: dwblogHelper.sh]](./codes/dwblogHelper.sh "BASH is simple and better")。

 執行範例程式。
 ```bash
  $ ./dwblogHelper.sh NSX-Microsegement-Depolyment.notes
 ```

 可以發現程式按照下載清單進行下載及轉換作業，最後將結果顯示。

 ![005.png](./pics/005.png)

 使用檔案管理查詢指定目錄存放轉換的PDF檔案。

 ![006.png](./pics/006.png)

8. 開啟PDF檔案確認內容。

 ![007.png](./pics/007.png)

 收工！

#### [補充]

 將範例程式跟下載清單的目錄調整成以下架構。

 ![008.png](./pics/008.png)

---
## [更新]

- 暫時沒有
---
## 參考網站

- [wkhtmltopdf](https://wkhtmltopdf.org/ "wkhtmltopdf")
- [VMware Taiwan臉書網誌](https://www.facebook.com/vmwaretaiwan/notes/?ref=page_internal "Taiwan is No.1")

---
[[回到首頁]](./READMD.md "HOME")
